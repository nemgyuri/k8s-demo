# Kubernetes deployment demo - Self-hosted "vanilla" cluster

# Important
**This demo can be deployed and run on a self-hosted "vanilla" Kubernetes cluster. If you want to try this out on an Azure Kubernetes Cluster, you should use the files and instructions provided in `aks` branch instead.**

## Brief summary
This project demonstrates a sample Kubernetes deployment with a handful of resources used.
The project consists of the following manifests:
- `00-create-networking-ingress-controller-metallb.sh` creates the networking with Calico/Tigera, employing custom address range for pods, deploys NGINX ingress controller and MetalLB for loadbalancer functionality.
- `01-namespaces.yml` creates three distinct namespaces (one meant for generic testing, one for the HAProxy deployment and associated thingies and one for the famous Spring PetClinic application)
- `02-storageclass.yml` creates a storage class which can be used later on for PV-s
- `03-volumes.yml` creates the persistent volumes necessary for the sample deployments
- `04-pvc.yml` creates the PersistentVolumeClaims
- `05-configmap.yml` creates four ConfigMaps, one for demonstrating the capabilities of ConfigMaps, one which holds some environment variables necessary for PetClinic, one which contains a sample HAProxy configuration and one which only contains envvars, but no "file content"
- `06-secrets.yml` creates two Secrets, one for demonstration purposes, one which holds the PetClinic app's sensitive data.
- `07-deployments.yml` creates four deployments.
  - Deployment of PetClinic application. This deployment consists of one "PetClinic" pod with two containers (app and MySQL database) inside. It isn't scaled, has only one instance.
  - Deployment of a generic NGINX container. This deployment also demonstrates how to use all the environment variables from a ConfigMap via keyword `envFrom`. This deployment has two replicas.
  - Deployment of a simple Busybox container, used solely for demonstrating the capabilities of ConfigMaps and Secrets.
  - Deployment of a HAProxy pod which consists of two containers, a HAProxy container responsible for request forwarding / "load balancing" and an NGINX "hello world" container which acts as a backend to the HAProxy for demonstration.
- `08-services.yml` creates the necessary Services for the deployments (NGINX, sample HAProxy deployment and PetClinic)
- `09-ingress.yml` creates the Ingresses necessary for NGINX, HAProxy and PetClinic deployments. An Ingress is mandatory if one wants to reach a Service from outside a cluster.
- `10-metallb.yml` creates the necessary objects for running an "external" loadbalancer via MetalLB (IP adress pool and layer 2 advertisement).

## How to try out
As a prerequisite you need a Kubernetes cluster up and running. You can create one using [my snippet](https://gitlab.com/-/snippets/2082525).
Once the cluster is online, you can simply clone this repo and
- **IMPORTANT**: run firstly the provided script `00-create-networking-ingress-controller-metallb.sh` for creating the necessary prerequisites.
Once the script finished its work, you can
- either apply the manifests one by one via `kubectl apply -f <filename>` / delete them via `kubectl delete -f <filename>`
- or use the provided helper scripts `apply-all.sh` and `delete-all.sh` for applying/deleting all of them at once.
