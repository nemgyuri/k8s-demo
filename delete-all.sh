#!/usr/bin/env bash

MANIFESTS=(
    "01-namespaces.yml" \
    "02-storageclass.yml" \
    "03-volumes.yml" \
    "04-pvc.yml" \
    "05-configmap.yml" \
    "06-secrets.yml" \
    "07-deployments.yml" \
    "08-services.yml" \
    "09-ingress.yml" \
    "10-metallb.yml"
    )

for m in "${MANIFESTS[@]}";
    do kubectl delete -f ./"${m}";
done